// ==UserScript==
// @name        Gitlab board enhancements
// @author      Drew
// @homepage    https://gitlab.com/drewgle.me
// @source      https://gitlab.com/drewgle.me/gitlab-board-improvements/
// @namespace   gitlab.com/drewgle.me
// @description Allows collasping all columns of a gitlab board
// @icon        https://assets.gitlab-static.net/assets/gitlab_logo-7ae504fe4f68fdebb3c2034e36621930cd36ea87924c11ff65dbcb8ed50dca58.png
// @version     1
// @match       *://gitlab.com/*/boards
// @match       *://gitlab.com/*/boards?*
// @grant       none
// ==/UserScript==


waitForElementToDisplay(".board", 500, function(){
  $(".board.is-draggable").addClass("is-expandable");
  $(".board.is-draggable .board-title").prepend("<i aria-hidden='true' class='fa fa-fw board-title-expandable-toggle fa-caret-down gmCollaspe'></i>");
  $(".board.is-draggable").removeClass("is-draggable");
  $(".board.is-draggable").removeClass("is-draggable");
  $(".gmCollaspe").data("expand", "expanded");
  
  $(".gmCollaspe").closest("header").click(function(){
    let arrow = $(this).find(".gmCollaspe");
    let board = arrow.closest(".is-expandable");
    if(arrow.data("expand") === "expanded") {
      arrow.data("expand", "collapsed");
      arrow.removeClass("fa-caret-down");
      board.addClass("is-collapsed");
      arrow.addClass("fa-caret-right");
    } else {
      arrow.data("expand", "expanded");
      arrow.removeClass("fa-caret-right");
      board.removeClass("is-collapsed");
      arrow.addClass("fa-caret-down");
    }
  });
});

function waitForElementToDisplay(selector, time, fn) {
        if(document.querySelector(selector)!=null) {
            fn();
        }
        else {
            setTimeout(function() {
                waitForElementToDisplay(selector, time, fn);
            }, time);
        }
    }